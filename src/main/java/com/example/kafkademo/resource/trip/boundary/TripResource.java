package com.example.kafkademo.resource.trip.boundary;


import com.example.kafkademo.resource.trip.control.TripResourceCtrl;
import com.example.kafkademo.resource.trip.entity.TripModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/trip")
public class TripResource {

    private TripResourceCtrl tripResourceCtrl;

    @Autowired
    public TripResource(TripResourceCtrl tripResourceCtrl) {
        this.tripResourceCtrl = tripResourceCtrl;
    }

    @GetMapping("/all")
    ResponseEntity<List<TripModel>> getAllTrips(){
        return ResponseEntity.ok(tripResourceCtrl.getAllTrips());
    }

    @GetMapping("/all/timeout")
    ResponseEntity<List<TripModel>> timeoutGetAllTrips(){
        return ResponseEntity.ok(tripResourceCtrl.timeoutRequest());
    }

    @GetMapping("/all/fail")
    ResponseEntity<List<TripModel>> failGetAllTrips(){
        return ResponseEntity.ok(tripResourceCtrl.failRequest());
    }
}
