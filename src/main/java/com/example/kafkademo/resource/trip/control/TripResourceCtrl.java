package com.example.kafkademo.resource.trip.control;

import com.example.kafkademo.resource.trip.entity.TripModel;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class TripResourceCtrl {

    private final List<TripModel> allTrips = List.of(
      new TripModel("Nasr City", "Sheraton"),
      new TripModel("New Cairo", "Madinaty"),
      new TripModel("Maadi", "Mohandseen")
    );

    public List<TripModel> getAllTrips(){
        return allTrips;
    }

    public List<TripModel> timeoutRequest() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return allTrips;
    }

    public List<TripModel> failRequest() {
        throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE);
    }
}
